authenticator
-------------

Introduction
------------

Authenticator provides a "google authenticator" authentication like.

Given two private token on each side and a date, generate a code which
should be the same on both sides according to the time.

Implementation comes from examples given at https://medium.com/@tilaklodha/google-authenticator-and-how-it-works-2933a4ece8c2

Author
------

SOARES Lucas <lucas.soares.npro@gmail.com>

https://gitlab.com/M2UCP/authentication/authenticator.git

