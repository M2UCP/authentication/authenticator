package authenticator

import (
	"errors"
	"fmt"
	"gitlab.com/M2UCP/authentication/smartcard"
)

type TokenManager struct {
	// card handler
	cardHandler *card.Handler

	// card
	card *card.Card

	// token info
	serverToken string
	clientToken string
	isTokenValid bool

	// mode
	mode CardMode
}

// build token manager
func BuildTokenManager( cardHandler *card.Handler,
	mode CardMode ) *TokenManager {
	// allocate
	tokenManager := &TokenManager{
		cardHandler: cardHandler,
		mode: mode,
	}

	// done
	return tokenManager
}

func ( tokenManager *TokenManager ) Update( ) {
	if tokenManager.card != nil {
		if !tokenManager.card.IsAlive( ) {
			tokenManager.card = nil
			tokenManager.isTokenValid = false
			fmt.Println( "[TOKEN MANAGER] card has been removed, token are no longer usable" )
		}
	} else {
		if lastCard := tokenManager.cardHandler.GetLastConnectedCard(); lastCard != nil {
			if cardMode, err := lastCard.SendRead( 0x10,
				4 ); err == nil {
				if CardMode( cardMode[ 0 ] ) == tokenManager.mode &&
					CardMode( cardMode[ 3 ] ) == tokenManager.mode {
					if serverToken, err := lastCard.SendRead(0x14,
						16); err == nil {
						if clientToken, err := lastCard.SendRead(0x18,
							16); err == nil {
							if CalculateChecksum( serverToken ) == cardMode[ 1 ] &&
								CalculateChecksum( clientToken ) == cardMode[ 2 ] {
								// notify
								fmt.Println( "[TOKEN MANAGER] token check sums match" )
								// save data
								tokenManager.serverToken = string(serverToken)
								tokenManager.clientToken = string(clientToken)
								tokenManager.isTokenValid = true
								tokenManager.card = lastCard

								// notify
								fmt.Println("[TOKEN MANAGER] extract from smartcard token server(",
									tokenManager.serverToken,
									"), client(",
									tokenManager.clientToken,
									")")
							} else {
								fmt.Println( "[TOKEN MANAGER] error with token checksum (given on card/expected), server(",
									CalculateChecksum( serverToken ), "/", cardMode[ 1 ], "), client(",
									CalculateChecksum( clientToken ), "/", cardMode[ 2 ], ")" )
							}
						} else {
							fmt.Println("[TOKEN MANAGER] can't read client token:",
								err)
						}
					} else {
						fmt.Println("[TOKEN MANAGER] can't read server token:",
							err)
					}
				} else {
					fmt.Println( "[TOKEN MANAGER] inserted card mode (",
						cardMode,
						") is not the expected one (",
						tokenManager.mode,
						")" )
				}
			} else {
				fmt.Println( "[TOKEN MANAGER] can't read card mode",
					err )
			}
		}
	}
}

// get OTP
func ( tokenManager *TokenManager ) getOTP( privateToken string ) ( string, error ) {
	// check token
	if !tokenManager.isTokenValid {
		return "", errors.New( "private token is invalid" )
	}

	// calculate token
	if token, err := GetTOTPToken( privateToken ); err == nil {
		return token, nil
	} else {
		return "", err
	}
}

// get client OTP
func ( tokenManager *TokenManager ) GetClientOTP( ) ( string, error ) {
	return tokenManager.getOTP( tokenManager.clientToken )
}

// get server OTP
func ( tokenManager *TokenManager ) GetServerOTP( ) ( string, error ) {
	return tokenManager.getOTP( tokenManager.serverToken )
}

// is card inserted?
func ( tokenManager *TokenManager ) IsCardInserted( ) bool {
	return tokenManager.isTokenValid
}
