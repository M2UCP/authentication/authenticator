package authenticator

const(
	// opad/ipad header names
	OPadHTTPHeader = "X-OPad"
	IPadHTTPHeader = "X-IPad"
	QPadHTTPHeader = "X-QPad"

	// m2m
	OTPHTTPHeader = "X-OTP"

	// session ID header
	SessionIDHTTPHeader = "X-SessionID"

	// card id length
	CardIDLength = 16
	CardPasswordLength = 16
)