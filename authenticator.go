package authenticator

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base32"
	"encoding/binary"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

const(
	// token length
	PrivateTokenLength = 16

	// token validity period in seconds
	TokenValidityDuration = 30
)

type CardMode byte
const(
	CardModeM2MServer = CardMode( 0x5E )
	CardModeM2MClient = CardMode( 0xCC )
	CardModeClient    = CardMode( 0xC1 )
)

// generate a private token
func GeneratePrivateToken( ) string {
	// authorized characters are numbers
	authorizedCharacterList := "234567ABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// fill string
	output := ""
	for i := 0; i < PrivateTokenLength; i++ {
		output += string( authorizedCharacterList[ rand.Int( ) % len( authorizedCharacterList ) ] )
	}

	// done
	return output
}

// generate code from private token
func GetHOTPToken(secret string, interval int64) ( string, error ) {
	//Converts secret to base32 Encoding. Base32 encoding desires a 32-character
	//subset of the twenty-six letters A–Z and ten digits 0–9
	if key, err := base32.StdEncoding.DecodeString(strings.ToUpper(secret)); err == nil {
		bs := make([]byte, 8)
		binary.BigEndian.PutUint64(bs, uint64(interval))
		//Signing the value using HMAC-SHA1 Algorithm
		hash := hmac.New(sha1.New, key)
		hash.Write(bs)
		h := hash.Sum(nil)
		// We're going to use a subset of the generated hash.
		// Using the last nibble (half-byte) to choose the index to start from.
		// This number is always appropriate as it's maximum decimal 15, the hash will
		// have the maximum index 19 (20 bytes of SHA1) and we need 4 bytes.
		o := (h[19] & 15)
		var header uint32
		//Get 32 bit chunk from hash starting at the o
		r := bytes.NewReader(h[o : o+4])
		if err = binary.Read(r, binary.BigEndian, &header); err == nil {
			//Ignore most significant bits as per RFC 4226.
			//Takes division from one million to generate a remainder less     than < 7 digits
			h12 := (int(header) & 0x7fffffff) % 1000000
			//Converts number as a string
			otp := strconv.Itoa(int(h12))
			return otp, nil
		} else {
			return "", err
		}
	} else {
		return "", err
	}
}

func GetTOTPToken( secret string ) ( string, error ) {
	// notify
	fmt.Print( "[OTP TOKEN] for token ",
		secret,
		" at timestamp=",
		time.Now( ).Unix( ) )

	if otp, err := GetHOTPToken( secret, time.Now().Unix() / TokenValidityDuration ); err == nil {
		// notify
		fmt.Println( ", generate OTP [",
			otp,
			"]" )

		// done
		return otp, nil
	} else {
		// notify
		fmt.Println( ", couldn't get OTP:",
			err )

		// done
		return "", err
	}
}