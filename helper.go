package authenticator

import(
	"crypto"
	_ "crypto/md5"
	"encoding/hex"
	"math/rand"
)

// calculate checksum with ovious overflow but to
// be used for minimal assertion of data validity
// when reading card
func CalculateChecksum( data [ ]byte ) byte {
	output := byte( 0 )
	for _, d := range data {
		output += d
	}
	return output
}

// hash data with single padding
func HashData( data string,
	pad HashPad ) string {
	m := crypto.MD5.New( )
	m.Write( [ ]byte( pad ) )
	m.Write( [ ]byte( data ) )
	return hex.EncodeToString( m.Sum( nil ) )
}

// hash data with ipad/opad
func HashDataDouble( data string,
	ipad, opad HashPad ) string {
	return HashData( HashData( data,
			opad ),
		ipad )
}

// hash data with qpad/ipad/opad
func HashDataTripple( data string,
	ipad, opad, qpad HashPad ) string {
	return HashData( HashDataDouble( data,
			ipad,
			opad ),
		qpad )
}

const(
	HashPadLength = 8
)

var(
	HashPadCharacterList = "azertyuiopqsdfghjklmwxcvbn1234567890AZERTYUIOPQSDFGHJKLMWXCVBN"
)

type HashPad string
func ( hashPad *HashPad ) Generate( ) {
	*hashPad = ""
	for i := 0; i < HashPadLength; i++ {
		*hashPad += HashPad( HashPadCharacterList[ rand.Int( ) % len( HashPadCharacterList ) ] )
	}
}